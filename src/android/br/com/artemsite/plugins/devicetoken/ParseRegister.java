package br.com.artemsite.tonolucro;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;


/**
 * Created by euclides@artemsite.com.br on 29/08/14.
 */
public class ParseRegister extends Application {
	
	public String APPLICATION_ID = "XXXXXXXXXXXXXXXX";
	public String CLIENT_KEY = "XXXXXXXXXXXXXXXX";    
	
	public ParseRegister(){

    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Log.d("PARSE","CONCLUIU");
        Parse.initialize(this, APPLICATION_ID, CLIENT_KEY);
        new ParseOperations().execute();
		//parse();
		/*Intent intent = new Intent(this, GovernancaDigital.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivity(intent);*/
    }
    public void parse(){
        try {
            Parse.initialize(this, APPLICATION_ID, CLIENT_KEY);
            PushService.setDefaultPushCallback(this, Tonolucro.class);
            ParseUser.enableAutomaticUser();
            ParseACL defaultACL = new ParseACL();

            // If you would like all objects to be private by default, remove this line.
            defaultACL.setPublicReadAccess(true);

            ParseACL.setDefaultACL(defaultACL, true);
            ParseInstallation.getCurrentInstallation().saveInBackground();
            Log.d("PARSE","CONCLUIU");
        }catch (Exception ex){
            Log.d("ERRO_PARSE",ex.getMessage());
        }
    }

    private class ParseOperations extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            parse();
            return null;
        }
    }

    public Context getContext(){
        return this;
    }
}
