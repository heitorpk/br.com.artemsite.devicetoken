package br.com.artemsite.devicetoken;

import android.util.Log;

import com.parse.ParseInstallation;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class DeviceToken extends CordovaPlugin{
    private String token;
    private JSONArray canais;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        token = null;
        canais = new JSONArray();

        try{
            token = ParseInstallation.getCurrentInstallation().getString("deviceToken");
            canais = ParseInstallation.getCurrentInstallation().getJSONArray("channels");
        }catch (Exception e) {Log.d("EXCEÇÃO TOKEN:", e.getMessage());}

    }

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.d("ENTROU", "Execute");
        if (action.equals("deviceToken")) {
            this.deviceToken(callbackContext);
            return true;
        }
        else if (action.equals("canais")) {
            this.canais(callbackContext);
            return true;
        }
        else if (action.equals("inscrever")) {
            this.inscrever(args, callbackContext);
            return true;
        }
        else if (action.equals("setIdUsuario")) {
            this.setIdUsuario(args, callbackContext);
            return true;
        }
        else
            return false;
    }

    private void deviceToken(CallbackContext callbackContext) {
        Log.d("TOKEN: ",token);
        if (token != null && token.length() > 0) {
            callbackContext.success(token);
        } else {
            callbackContext.error("");
        }
    }

    private void canais(CallbackContext callbackContext) {
        if (canais != null) {
            callbackContext.success(canais);
        } else {
            callbackContext.error("");
        }
    }

    private void inscrever(JSONArray channels,CallbackContext callbackContext) throws JSONException {
        String canal = ""+channels.get(0);
        Log.d("CANAIS",canal);


        List<String> canaisLista = new ArrayList<String>(Arrays.asList(canal.split(",")));

        Set bkp = new LinkedHashSet(canaisLista);
        canaisLista.clear();
        canaisLista.addAll(bkp);

        Log.d("CANAIS",canal);

        if (canaisLista.size() > 0) {
            ParseInstallation.getCurrentInstallation().remove("channels");
            ParseInstallation.getCurrentInstallation().saveInBackground();

            for(int i=0;i<canaisLista.size();i++){
                ParseInstallation.getCurrentInstallation().add("channels", canaisLista.get(i));
            }

            ParseInstallation.getCurrentInstallation().saveInBackground();
            callbackContext.success();
        } else {
            callbackContext.error("");
        }
    }

    private void setIdUsuario(JSONArray id_usuario,CallbackContext callbackContext) throws JSONException {
        Log.d("ENTROU", "SetUsuario");
        String id_user= ""+id_usuario.get(0);

        Log.d("ID USUARIO",id_user);
        Log.d("ID USUARIO OBJETO", id_usuario.toString());

        if (id_user != null) {
            ParseInstallation.getCurrentInstallation().remove("idUsuario");
            ParseInstallation.getCurrentInstallation().saveInBackground();
            ParseInstallation.getCurrentInstallation().addUnique("idUsuario", id_user);
            //ParseInstallation.getCurrentInstallation().put("id_usuario", id_user);
            ParseInstallation.getCurrentInstallation().saveInBackground();

            callbackContext.success();
        } else {
            callbackContext.error("Id não armazenada.");
        }
    }
}