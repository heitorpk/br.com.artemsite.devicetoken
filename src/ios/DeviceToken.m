//
//  DeviceToken.m
//  Cleber Toledo
//
//  Created by Euclides Junior on 24/06/14.
//
//

#import "DeviceToken.h"
#import <Parse/Parse.h>

@implementation DeviceToken

@synthesize canais, tokenString;
-(id)init
{
    PFInstallation *currentInstallation ;
    currentInstallation = [PFInstallation currentInstallation];
    return self;
}

-(void)deviceToken:(CDVInvokedUrlCommand*)command
{
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceTokenGlobal"];
    
    CDVPluginResult* pluginResult = nil;
    
    if([tokenString length] > 0){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:tokenString];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}

-(void)canais:(CDVInvokedUrlCommand*)command
{
    canais = [PFInstallation currentInstallation].channels;
    CDVPluginResult* pluginResult = nil;
    
    if(canais!=nil)
    {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:canais];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@""];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)inscrever:(CDVInvokedUrlCommand*)command
{
    PFInstallation *currentInstallation ;
    currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation removeObjectForKey:@"channels"];
    [currentInstallation saveInBackground];
    
    CDVPluginResult* pluginResult = nil;
    NSArray* canaisMarcados = [command.arguments objectAtIndex:0];
    
    if(canaisMarcados != nil)
    {
        NSLog(@"CANAL %@", canaisMarcados);
        [currentInstallation addUniqueObject:canaisMarcados forKey:@"channels"];
        
        [currentInstallation saveInBackground];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)setIdUsuario:(CDVInvokedUrlCommand*)command
{
    PFInstallation *currentInstallation ;
    currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation removeObjectForKey:@"idUsuario"];
    [currentInstallation saveInBackground];
    
    CDVPluginResult* pluginResult = nil;
    NSString* idUsuario_html = [command argumentAtIndex:0];
    //NSArray* idUsuario_html = [command.arguments objectAtIndex:0];
    NSString* arrayItem;
    
    if(idUsuario_html != nil)
    {
        [currentInstallation addUniqueObject:arrayItem forKey:@"idUsuario"];
        [currentInstallation saveInBackground];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@""];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end