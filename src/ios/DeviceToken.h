//
//  DeviceToken.h
//  Cleber Toledo
//
//  Created by Euclides Junior on 24/06/14.
//
//

#import <Cordova/CDV.h>

@interface DeviceToken : CDVPlugin

@property (nonatomic, strong) NSArray* canais;
@property (nonatomic, strong) NSString *tokenString;
@property (retain) CDVInvokedUrlCommand * command;

-(void)devicetoken:(CDVInvokedUrlCommand*)command;
-(void)canais:(CDVInvokedUrlCommand*)command;
-(void)inscrever:(CDVInvokedUrlCommand*)command;
-(void)setIdUsuario:(CDVInvokedUrlCommand*)command;

@end