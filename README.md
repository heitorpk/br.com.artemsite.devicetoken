Plugin Cordova DeviceToken
=========================



Plugin para pegar o token de ativação de mensagens push gerados pelo GCM e APNS após a instalação do Parse.

Instalação
==

Antes de instalar o plugin é necessário configurar o SDK do parse para cada plataforma onde ele será executado, mais detalhes na página oficial: https://parse.com/tutorials
 
Execute a instalação do plugin via terinal:

```sh
cordova plugin add git@bitbucket.org:euclides1908/br.com.artemsite.devicetoken.git
```

Configuração
==

Android
--

- Altere o arquivo `ParseRegister.java` dentro do pacote do plugin e insira as credenciais do Parse nas linhas: 

```java
public String APPLICATION_ID = "XXXXXXXXXXXXXXXX";
public String CLIENT_KEY = "XXXXXXXXXXXXXXXX";
```

- Após isso, mova o arquivo `ParseRegister.java` do seu pacote original `br.com.artemsite.devicetoken` para dentro do pacote onde se econtra a classe principal do projeto.

>Nota: É necessário modificar o `AndroidManifest.xml` para que o `ParseRegister.java` seja o aplicativo padrão a ser chamado quando houver algum evento do Receiver e que sua classe principal controle a Activiy inicial do seu aplicativo, conforme exemplo abaixo do aplicativo Tonolucro:

```xml
<?xml version='1.0' encoding='utf-8'?>
<manifest android:hardwareAccelerated="true" 
    android:versionCode="4" 
    android:versionName="1.0" 
    package="br.com.artemsite.tonolucro" 
    xmlns:android="http://schemas.android.com/apk/res/android">
    
    <supports-screens 
        android:anyDensity="true" 
        android:largeScreens="true" 
        android:normalScreens="true" 
        android:resizeable="true" 
        android:smallScreens="true" 
        android:xlargeScreens="true" />
    
     <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    
    <permission android:name="br.com.artemsite.tonolucro.permission.C2D_MESSAGE"
        android:protectionLevel="signature" />
    <uses-permission android:name="br.com.artemsite.tonolucro.permission.C2D_MESSAGE" />
    
    <application 
        android:hardwareAccelerated="true" 
        android:icon="@drawable/ic_launcher" 
        android:label="@string/app_name"
        android:name=".ParseRegister">
        
        <activity android:configChanges="orientation|keyboardHidden|keyboard|screenSize|locale" android:label="@string/app_name" android:name=".Tonolucro" android:theme="@android:style/Theme.Black.NoTitleBar" android:screenOrientation="portrait">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        
        <service android:name="com.parse.PushService" />
        <receiver android:name="com.parse.ParseBroadcastReceiver">
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED" />
                <action android:name="android.intent.action.USER_PRESENT" />
            </intent-filter>
        </receiver>
        <receiver android:name="com.parse.GcmBroadcastReceiver"
            android:permission="com.google.android.c2dm.permission.SEND">
            <intent-filter>
                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
                <action android:name="com.google.android.c2dm.intent.REGISTRATION" />
                <category android:name="br.com.artemsite.tonolucro" />
            </intent-filter>
        </receiver>
    </application>
    
    <uses-sdk android:minSdkVersion="10" android:targetSdkVersion="19" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
</manifest>

```

iOS
--
//TODO 
░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░ 

░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░ 

░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░ 

░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░ 

░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░ 

█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█ 

█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█ 

░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░ 

░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░ 

░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░ 

░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░ 

░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░ 

░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░ 

░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░ 

░░░░░░░░░░░░░░▀▄▄▄▄▄░░░░░░░░█░░ 


Versão
----

1.0


Licença
----

GNU


**Free Software, Hell Yeah!**