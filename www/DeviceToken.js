cordova.define("br.com.artemsite.devicetoken.DeviceToken", function(require, exports, module) {
    var exec = require('cordova/exec');


    var DeviceToken = function() { };

    DeviceToken.getInfo = function (successCallback, errorCallback) {
      exec(successCallback, errorCallback, "DeviceToken", "deviceToken", []);
    };

    DeviceToken.getCanais = function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "DeviceToken", "canais", []);
    };

    DeviceToken.setCanais = function (canais, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "DeviceToken", "inscrever", [canais]);
    };

    DeviceToken.setIdUsuario = function (id_usuario, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "DeviceToken", "setIdUsuario", [id_usuario]);
    };

    module.exports = DeviceToken;
});